sphinx==7.2.6

# Sphinx dependencies that are important
Jinja2==3.1.3
Pygments==2.17.2
docutils==0.20.1
snowballstemmer==2.2.0
babel==2.14.0
requests==2.31.0

# Only needed for building translations.
sphinx-intl==2.1.0

# Only needed to match the theme used for the official documentation.
# Without this theme, the default theme will be used.
furo==2024.1.29
sphinx-basic-ng==1.0.0b2

# Only for convenience, allows live updating while editing RST files.
# Access by running:
#   make livehtml
sphinx-autobuild==2024.2.4

# Required for spell-checking
pyenchant
