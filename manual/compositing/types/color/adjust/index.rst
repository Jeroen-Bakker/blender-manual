
##########
  Adjust
##########

.. toctree::
   :maxdepth: 1

   bright_contrast.rst
   color_balance.rst
   color_correction.rst
   exposure.rst
   gamma.rst
   hue_correct.rst
   hue_saturation.rst
   rgb_curves.rst
   tone_map.rst
