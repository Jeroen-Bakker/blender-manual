
******
Rotate
******

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Rotate`

Rotates geometry in the direction in which the cursor is moved.
The initial drag direction is the zero angle
and by rotating around the center you can create a vortex/swirl effect.


Brush Settings
==============

General
-------

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.
