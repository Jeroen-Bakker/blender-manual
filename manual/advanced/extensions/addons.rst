.. Mark as "orphan" until extensions is out of beta.

.. index:: Add-ons Extensions
.. index:: Add-ons
.. .. _bpy.types.Addon:
.. .. _bpy.ops.wm.addon:
.. .. _bpy.types.WindowManager.addon:

*******
Add-ons
*******

*Add-ons* let you extend Blender's functionality using Python.
Most of the time you can get add-ons as part of the :doc:`Extensions <index>` system.

.. tip::

   If the Add-on does not activate when enabled,
   check the :doc:`Console window </advanced/command_line/introduction>`
   for any errors that may have occurred.

User-Defined Add-on Path
========================

You can also create a personal directory containing new add-ons and configure your files path in
the *File Paths* section of the *Preferences*. To create a personal script directory:

#. Create an empty directory in a location of your choice (e.g. ``my_scripts``).
#. Add a subdirectory under ``my_scripts`` called ``addons``
   (it *must* have this name for Blender to recognize it).
#. Open the *File Paths* section of the *Preferences*.
#. Set the *Scripts* file path to point to your script directory (e.g. ``my_scripts``).
#. Save the preferences and restart Blender for it to recognize the new add-on location.

Now when you install add-ons you can select the *Target Path* when installing 3rd party scripts.
Blender will copy newly installed add-ons under the directory selected in your Preferences.

Bundle Dependencies
===================

For add-ons to be bundled as extensions, they must be self-contained.
That means they must come with all its dependencies. In particular 3rd party Python modules.

They are a few options for this:

Bundle with :doc:`Python Wheels <./python_wheels>`.
   This is the recommended way to bundle dependencies.

Bundle other add-ons together.
   This is recommended if an add-on depends on another add-on.

   Make sure that both the individual and the combined add-on
   check for already registered types (Operators, Panels, ...).
   This avoids duplication of operators and panels on the interface
   if the add-ons are installed as a bundle and individually.

Bundle with `Vendorize <https://pypi.org/project/vendorize>`__
   This can be used as a way to bundle a pure Python dependencies as a sub-module.

   This has the advantage of avoiding version conflicts although it requires some work to setup each package.

Legacy vs Extension Add-ons
===========================

With the introduction of Extensions in Blender 4.2, the old way of creating add-ons is considered deprecated.
While the changes are rather small they impact existing add-ons.

In order to allow a smooth transition process, the so-called legacy add-ons will continue to be supported by Blender.
They can be installed via :doc:`Install legacy Add-on </editors/preferences/extensions>` button in the User
Preferences.

All add-on maintainers are urged to convert the add-ons they want to share, so they are future proof and can support
features like updating from the extensions platform.

Converting a Legacy Add-on into an Extension
--------------------------------------------

#. Create a :doc:`manifest file <./getting_started>`.
#. Remove the ``bl_info`` information (this is now in the manifest).
#. Replace all references to the module name to ``__package__``.
#. Make all module imports to use relative import.
#. Use `wheels <https://pythonwheels.com/>`__ to pack your external Python dependencies.
#. Remember to test it thoroughly.

.. note::

   For testing it is import to :doc:`install the extension from disk </editors/preferences/extensions>` and check if
   everything is working well. This will get you as close to the final experience as possible.

Extensions and Namespace
------------------------

The legacy add-ons would use their module name to access the preferences. This could lead to a name clash when
extensions with the same name (from different repositories) would be installed.
To prevent this conflict, the repository name is now part of the namespace.

For example, now instead of ``kitsu`` the module name would be ``bl_ext.{repository_module_name}.kitsu`` instead.

This has a few implications for preferences and module imports.

User Preferences and ``__package__``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Add-ons can define their own preferences which use the package name as an identifier.
This can be accessed using ``__package__``.

This was already supported in the legacy add-ons, but not reinforced.
As such this can break backward compatibility.

Before:

   .. code-block:: python

      class KitsuPreferences(bpy.types.AddonPreferences):
          bl_idname = "kitsu"
          # ... snip ...

      # Access with:
      addon_prefs = bpy.context.preferences.addons["kitsu"]

Now:

   .. code-block:: python

      class KitsuPreferences(bpy.types.AddonPreferences):
          bl_idname = __package__
          # ... snip ...

      # Access with:
      addon_prefs = bpy.context.preferences.addons[__package__]

Sub-packages
   An add-on that defines sub-packages (sub-directories with their own ``__init__.py`` file)
   that needs to use this identifier will have to import the top-level package using a relative import.

   .. code-block:: python

      from .. import __package__ as base_package

   Then ``base_package`` can be used instead of ``__package__``.
   The ``..`` imports relative to the packages parent, sub-sub-packages must use ``...`` and so on.

.. note::

   - The value of ``__package__`` will vary between systems so it should never be replaced with a literal string.
   - Extensions must not manipulate the value of ``__package__`` as this may cause unexpected behavior or errors.


Relative Imports
^^^^^^^^^^^^^^^^

   :before: ``from kitsu import utils``
   :now: ``from . import utils``

Importing packages within the add-on module need to use relative paths.
This is a standard Python feature and only applicable for add-ons that have multiple folders.

This was already supported in the legacy add-ons, but not reinforced. As such this can break backward compatibility.
