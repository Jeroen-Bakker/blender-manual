.. index:: Tags

***************
Extensions Tags
***************

A different set of tags is available for the different extensions types.
This is the list of the tags currently supported:

Add-ons
=======

* 3D View
* Add Curve
* Add Mesh
* Animation
* Bake
* Camera
* Compositing
* Development
* Game Engine
* Geometry Nodes
* Grease Pencil
* Import-Export
* Lighting
* Material
* Modeling
* Mesh
* Node
* Object
* Paint
* Pipeline
* Physics
* Render
* Rigging
* Scene
* Sculpt
* Sequencer
* System
* Text Editor
* Tracking
* User Interface
* UV

Themes
======

* Dark
* Light
* Colorful
* Inspired By
* Print
* Accessibility
* High Contrast
